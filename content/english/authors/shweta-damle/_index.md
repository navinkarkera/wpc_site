---
title: "Shweta Damle"
# page title background image
bg_image: "images/backgrounds/page-title.jpg"
# meta description
description : "Chandan Kumar is the coordinator of the Working People’s Charter, a network of more than 150 provincial, local organizations of informal workers, founded in 2013. He is also a member of National Minimum Wage Advisory Board GOI."
email: "email2@example.org"
# portrait
image: ""
social:
  - icon : "ti-facebook" # themify icon pack : https://themify.me/themify-icons
    link : "#"
  - icon : "ti-twitter-alt" # themify icon pack : https://themify.me/themify-icons
    link : "#"
  - icon : "ti-github" # themify icon pack : https://themify.me/themify-icons
    link : "#"
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sit amet vulputate augue. Duis auctor lacus id vehicula gravida. Nam suscipit vitae purus et laoreet.
Donec nisi dolor, consequat vel pretium id, auctor in dui. Nam iaculis, neque ac ullamcorper.
