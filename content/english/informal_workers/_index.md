---
title: Informal Workers
bg_image: images/about/about-hero-image.jpg
description: The Indian workforce is characterized by a small formal / organized sector
  of the economy and a huge informal / unorganized sector, both in agriculture and
  non-agriculture.
menu:
  main:
    URL: informal_workers
    weight: 1

---
Employment in the informal sector is characterized by seasonality of employment, scattered workplace,
no employer-employee relationship, poor working conditions, irregular and often long working hours,
low-productivity and poor earnings, exposed to various forms of insecurity and health hazards,
limited access to credit, legal protection and government support and lack of social security.

The term `unorganized worker` has been defined under the Unorganized Workers Social Security Act, 2008 as
**a home-based worker, self-employed worker or a wage worker in the organized sector and includes a worker in the organized sector who is not covered by any of the Acts.**

Therefore unorganized workers are not limited to those who are working in the unorganized sector but also include such workers who are working in the formal sector without any social security cover.