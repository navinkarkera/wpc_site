---
title: Wpc Media Statements
bg_image: images/backgrounds/page-title.jpg
description: Our response on labour policy developments
menu:
  main:
    name: Media statements
    URL: media_statements
    weight: 4

---
