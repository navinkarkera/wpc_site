---
title: WPC STATEMENT ON LABOUR ORDINANCES PUSHED BY STATES
date: 2020-05-16T15:27:17.000+06:00
bg_image: images/backgrounds/page-title.jpg
description: The use of ordinances to justify these violations as “temporary measures”
  is unethical. These measures must go through strict legislative and legal scrutiny.
  We assert that the economic recovery of the country, which can only commence if
  it is in the interest of both workers as well as employers, will fail if it is built
  on the back of sacrificing the protections, rights and security of l workers. If
  we do not respond to this diligently and urgently, we fear that India might see
  a social unrest unprecedented in its independent history.
image: images/media_statements/statemet_on_labour.jpg
category: ''
type: media_statements

---
After watching the horrific and heart wrenching condition of workers on the streets of India in the wake of the long COVID-19 lock down,
it is with great alarm, sadness and dismay for us to learn that the governments of Uttar Pradesh and Madhya Pradesh have decided to dismantle their existing labour laws.
At least ten other states including Haryana, Himachal Pradesh, Gujarat, Rajasthan, Odisha, Assam, Maharashtra and Uttarakhand have officially increased working hours from 9 to 12 a day.
India’s labour laws have been a result, not only of persistent workers’ movements for over a century,
but also of the economic evidence proving the positive contribution of basic social security measures for workers to stable and sustainable economic growth.
Unequal and insecure working conditions put enormous socio-economic pressures on society, as it accentuates existing health, educational,
livelihood and social vulnerabilities.
The state is then forced to address them with further investment from its limited state coffers.

[ Sacrificing workers on altar of "development" ](https://workingpeoplescharter.in/documents/5/WPC_Statement_on_labour_ordinance_pushed_by_states-2020.pdf)

For more information please contact followings:
- Chandan Kumar - chandancampaign@gmail.com , M +91 9717891696
- Divya Varma-  divya.varma@aajeevika.org ,M +91 9606359333
- Nirmal Gorana - nirmalgorana@gmail.com, M +91 9899823256