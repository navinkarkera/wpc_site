---
title: राज्यों के श्रम अध्यादेशों पर डब्ल्यूपीसी का बयान
date: 2020-05-19T15:27:17.000+06:00
bg_image: images/backgrounds/page-title.jpg
description: राज्यों के श्रम अध्यादेशों पर डब्ल्यूपीसी का बयान
image: images/media_statements/sacrificing_workers.jpg
category: ''
type: media_statements

---
### [Sacrificing workers on altar of "development"- Hindi](https://workingpeoplescharter.in/documents/8/HIndi-_WPC_Statement_on_labour_ordinance_pushed_by_states-2020.pdf)

कोविड-19 के कारण लॉकडाउन लगाए जाने के बाद भारत की सड़कों पर श्रमिकों की हृदयविदारक स्थिति देखकर हम मर्माहत हैं और हमें
ज़्यादा पीड़ा इस बात से हुई है कि पहाड़ से लगने वाले इन दुखों के बीच मध्य प्रदेश और उत्तर प्रदेश की सरकारों ने वर्तमान श्रमिक क़ानूनों को
समाप्त करने का फ़ैसला किया है। इनके अलावा, कम से कम दस और राज्यों हरियाणा, हिमाचल प्रदेश, गुजरात, राजस्थान, ओडिशा, असम,
महाराष्ट्र और उत्तराखंड ने आधिकारिक रूप से काम करने के घंटे को 9 से बढ़ाकर 12 कर दिया है।

उत्तर प्रदेश सरकार ने इलाहाबाद हाईकोर्ट के नोटिस के बाद 12 घंटे की शिफ़्ट के कठोर प्रस्ताव को वापस ले लिया है। यह उन राज्यों को
महत्त्वपूर्ण संकेत है कि इस तरह के ग़ैरक़ानूनी और असंवैधानिक क़दमों को कामगार बर्दाश्त नहीं करेंगे। अन्य समस्याओं को देखते हुए यह एक
बहुत ही छोटी जीत है पर ऐसा सिर्फ़ विभिन्न श्रमिक संगठनों के लगातार अथक प्रयास के कारण ही सरकार इसे वापस लेने को बाध्य हुई है।

अध्यादेश की मदद लेकर इन उल्लंघनों को “तात्कालिक क़दम” बताकर इनको सही बताना अनैतिक है। इस तरह के क़दमों को कानूनी रास्तों
और कठोर वैधानिक जाँच की कसौटी पर आवश्यक रूप से कसा जाना चाहिए। हम इस बात को दुहराते हैं कि देश की आर्थिक व्यवस्था को
पटरी पर तभी लाया जा सकता है जब यह श्रमिक और नियोक्ता दोनों के हितों में है और श्रमिकों के संरक्षण, उनकी सुरक्षा, उनके अधिकारों को
समाप्त करके इसे हासिल करने की बात कभी सफल नहीं होगी। अगर हम इसके ख़िलाफ़ तुरंत और पूरे मनोयोग से खड़े नहीं होते हैं, तो हमें
डर है कि भारत को ऐसी सामाजिक अराजकता का सामना करना पड़ सकता है जो उसने आज़ादी के बाद इससे पहले कभी नहीं देखी है।