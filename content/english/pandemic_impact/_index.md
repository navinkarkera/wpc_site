---
title: Pandemic and the impact on informal labour
bg_image: images/about/about-hero-image.jpg
description: Although the Government of India and various state governments have made
  policies and interventions to help stranded migrants, however a majority of workers
  are unaware of government programmes or have not been reached by them.
menu:
  main:
    name: Pandemic & Impact
    URL: pandemic_impact
    weight: 3

---
{{< youtube fHFy-t0tFoY >}}

---

## [ Statement of Working Peoples' Charter ](https://workingpeoplescharter.in/documents/3/Statement_of_Working_Peoples_Charter__1.pdf)

The ILO reports that the COVID-19 crisis would push around 40 crore workers in
India's informal sector - comprising a bulk of the country's economy — deeper into poverty,
including ITS migrant workers.

---
## Media Coverage
The Working Peoples' Charter has issued a statement addressing the coronavirus crisis in India, and the workers affected by the lockdown.
**Measures must protect our people, in particular the most vulnerable -
including the elderly, the sick and the poor.**

---

## Policy Intervention

Although the Government of India and various state governments have made policies and interventions 
to help stranded migrants, a majority of workers are unaware of government programmes
or have not been reached by them. It is imperative given this situation that
both government policies are massively and rapidly strengthened in this regard,
even as communities come forward to help stranded migrants in need!

[Letter to Ms Nirmla Sitharaman, Chairperson Covid-19 Economic Task Force, GOI](http://workingpeoplescharter.in/documents/2/COVID-19_demand_letter_to_ERTF-_Circulation.pdf)

---
## [Statement of Working Peoples' Charter on Coronavirus crisis in India](https://www.thehindu.com/news/national/statement-of-working-peoples-charter-on-coronavirus-crisis-in-india/article31256395.ece)