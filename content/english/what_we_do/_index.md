---
title: What we do
bg_image: images/about/about-hero-image.jpg
description: Solidarity to migrant workers
menu:
  main:
    name: What We Do
    URL: what_we_do
    weight: 2

---
{{< image
src="/images/about/donation_call.png"
alt="what_we_do"
class="text-center"
>}}

**Working Peoples' Charter**- a network of more than 151 provincial, local organizations of informal workers, 
was founded in 2013 to fulfil this vacuum. The Charter believes in capacity building of network's members. 
This will help the members wage struggles at provincial and national level. 
The capacity building includes understanding of laws pertaining to informal workers,
including the proposed bills on wage, industrial relations, occupational health and safety 
and social security in the legislature. Currently, there is no such platform existing in India
voicing collective concerns of informal workers. This positioning of being the first 
and the only one network at national level gives it a competitive advantage.

The network has the grassroots presence in the place in more than eighteen states of India- Andhra Pradesh,
Assam, Bihar, Delhi, Karnataka, Maharashtra, Madhya Pradesh, Odisha, Punjab, Rajasthan, Telangana,  Uttar Pradesh,
Tamil Nadu, Punjab, Haryana, Uttarakhand, Assam  and West Bengal. It has a vast experience in negotiating with municipal,
state and national governments with the higher participation of both grassroots activists and workers.
With this context, the Working Peoples’ Charter envisions that every worker has a right to work and quality employment.

[The WPC Charter](https://workingpeoplescharter.in/documents/4/CHARTER.pdf)