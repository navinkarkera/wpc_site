---
title: "COLLECTIVE EFFORTS ON LEGAL INTERVENTION IN THE SUPREME COURT AND HIGH COURT."
date: 2020-06-12T15:27:17+06:00
draft: false
# page title background image
bg_image: "images/backgrounds/page-title.jpg"
# meta description
description : "This is an update with regards to our collective efforts on legal intervention in the Supreme Court and High Court.
                Earlier some of us have approached High Courts, and we have received some level of relief.
                But efforts made at the Supreme Court did not yield any concrete result.
                Most petitions either got adjourned or disposed of.
                However because of the crisis that arose due to #covid lockdown, forced the govt and the judiciary to take cognizance.
                We realized that there are two opportunities where intervention can be made in order to get substantial orders in favour of workers."
# post thumbnail
image: "images/blog/collective_efforts.jpg"
# post author
authors: ["Nirmal Gorana", "Chandan Kumar"]
# taxonomy
categories: []
tags: []
# type
type: "post"
---
### Friends and Comrades,

This is an **update with regards to our collective efforts on legal intervention in the Supreme Court and High Court**.
Earlier some of us have approached High Courts, and we have received some level of relief.
But efforts made at the Supreme Court did not yield any concrete result.
Most petitions either got adjourned or disposed of.

However because of the crisis that arose due to covid lockdown, forced the govt and the judiciary to take cognizance.
We realized that there are **two opportunities where intervention can be made
in order to get substantial orders in favour of workers** :

One when **central govt themselves failed to defend their own direction to protect wage theft**,
and subsequently employer association "Ficus Pax Private Limited" challenged govt order and expressed inability to pay wages.
A half hearted order came where the court was seen washing off their hands and refused to take any position.
The court just asked employees and employers to sit together and find a solution.
Whereas the central govt could not even defend their own position/directive.
We see this entire process as a gross violation of multiple supreme court jurisprudence and labour laws, and it might lead to institutionalising a bonded labour system.
It is important that the working class movement must come together and help the court to implement its own order.

**The court must direct those establishments which have sufficient capital/reserve to pay wages to all the workers for the period of lockdown.
Those industries (MSEMs) who claim to not have sufficient capital must produce their balance sheet to support their claim.
And for such industries the government of India must come out with a policy prescribing financial subsidy for them.**

This will definitely help the workers, industry and bring the economy back on track.
Similar efforts are being taken around the world, including lower income countries.

**In the case of self-employed/daily wagers, the government must play the role of principal employer and pay floor level minimum wages as prescribed in the newly legislated ' wage code act 2019'.
The benchmark should be at par with the ministry of labour expert committee recommendation @ Rs 375.**
This should be treated as guaranteed income.

To download detail order and get summary use the link below:

[Employer Challenging MHA Order](https://workingpeoplescharter.in/our-blogs/employer-challenging-mha-order-pay-wages-during-covid19lockdown/)

And secondly when a number of deaths happened across the country, it was not due to COVID, but state negligence.
At the same time, 20 senior lawyers of supreme court and high court wrote to the Chief Justice Of India and sought immediate intervention.
As an outcome of the same, **the apex court took up the matter of migrant workers as suo moto on 27th May**.

On the same day some of us met in the evening and formed a group of lawyers and organisations working with informal labour,
later a few more joined; Gayatri Singh, Indira Jaisingh, Anand Grover, Colin Gonsalves, Prashant Bhushan, Dinesh Dwivedi, Sanjay Parikh and others,
and Union/groups/researcher including Sarvahara Jan Andolan, Angmehnati Kashtakari Sangharsh Samiti, Aajeevika Bureau, MKSS, NCCEBL, NAPM, SWAN,
NHF, HRLN, YUVA, NLU Bangalore, Ravi Srivastava, Harsh Mander, Gautam Bhan, Ramapriya Gopalakrishnan, Rahul Sapkaal and others to help court.

Our collective efforts have resulted in getting some good orders.
In order to implement even these orders, will be a mammoth task.

To download detail order and get summary use the the link below:

[Supreme Court Interim Orders](https://www.workingpeoplescharter.in/media-statements/supreme-court-interim-orders-suo-moto-case-migrants/)

All those who like to become part of this effort, and help the case, can choose to do the following;
1. **Identification of industry/establishment/enterprises and geographical location** where wage workers are not paid wages or wages withheld.

2. Are there any **layoffs, retrenchments and closure of industry due to wage related demands**?

3. How many **self employed** including daily wage workers, home based workers, domestic workers, street vendors **need guaranteed income support**?


**As part of follow-up the following actions are suggested to be undertaken at states immediately on Suo Moto Matter on Migrnats**.

1. Representation to Chief Secretary of both the source and destination states.
A template letter is attached, which you can use, by incorporating details of your state.

    (The draft was worked on by Anjor Bhaskar )
    [Draft letter to chief secretaries](https://workingpeoplescharter.in/documents/22/Draft_Letter_to_Chief_Secretaries_of_States_.docx)

2. A **Joint Press Conference** by organizations & activists working on the rights of migrant workers,
at the earliest, highlighting the problems faced by migrant workers post the Supreme Court’s order and urging immediate implementation of the order.

3. **RTI application** be filed with your State Labour Department seeking **the status of implementation of the Inter-State Migrant Workers Act
during and before lockdown** as well as action taken on withdrawal of cases against workers as per the SC’s orders.
**(RTI Application Template developed by NAPM)**.

    [RTI Format on ISMA](https://workingpeoplescharter.in/documents/23/RTI_on_ISMWA.docx)

4. Make representation to the local District Magistrate / Collector for withdrawal of the cases against workers. The Supreme Court has categorically asked to withdraw all cases imposed on workers during lockdown.

5. **Translate Supreme Court Orders** in different languages and circulate it at the local level.
If not full order, then at least relevant part of the order (Para 35) –
    English, Hindi, Telugu, Kannada and Tamil summaries are available [here](https://www.workingpeoplescharter.in/media-statements/supreme-court-interim-orders-suo-moto-case-migrants/)  [Watch this space for more translations]

6. **Information-Compilation**: A large number of states have filed preliminary affidavits before the Supreme Court.
Supreme Court has asked Centre and states to file further affidavits by 24th June.
The next hearing is on 8th July. Please gather as much information as possible about the situation of migrant workers,
concrete case studies of violations, entitlements and health situations,
non-payment of wages, specific status of implementation of each of the directives of SC.

7. **Social Media**: Please write about the SC Orders and the status of implementation on your social media platforms.
In particular, **tag the concerned CMs and CSs on Twitter**, wherever you find gaps and refer to the relevant provisions of the Order.
You can also tag @napmindia, @Indiawpc @AajeevikaBureau  @StrandedWorkers, @migrant_IN (This idea is inspired from NAPM email)

**We will look forward to receiving your interest /ideas to move forward**

Best regards,

Chandan Kumar and Nirmal Gorana

https://www.counterview.net/2020/06/wage-non-payment-may-institutionalise.html

workerscharterprocess@gmail.com
