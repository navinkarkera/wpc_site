---
title: "EMPLOYER CHALLENGING #MHA ORDER TO PAY WAGES DURING #COVID19LOCKDOWN."
date: 2020-06-12T15:27:17+06:00
draft: false
# page title background image
bg_image: "images/backgrounds/page-title.jpg"
# meta description
description : 'Today Supreme Court batted for negotiations between establishments and workers on the full payment of wages,
                regardless of the direction issued by the Ministry of Home Affairs on March 29.
                "**No industry can survive without the workers. Thus employers and employee need to negotiate and settle among themselves.
                If they are not able to settle it among themselves, they need to approach the concerned labour authorities to sort the issues out**",
                Justice Bhushan read out from the order.
                '
# post thumbnail
image: "images/blog/employee_mha_order.jpg"
# post author
authors: ["Chandan Kumar"]
# taxonomy
categories: []
tags: []
type: post

---
### Update from supreme court on the employer challenging #MHA order to pay wages during #Covid19Lockdown.
##### Our ally "Angmehnati Kashtakari Sangharsh Samiti" is intervening in the matter....

Today Supreme Court batted for negotiations between establishments and workers on the full payment of wages, regardless of the direction issued by the Ministry of Home Affairs on March 29.

` No industry can survive without the workers.
Thus employers and employee need to negotiate and settle among themselves.
If the employer Challenging Govt order to pay minimum wages_Judgement_12-Jun-2020
are not able to settle it among themselves,
they need to approach the concerned labour authorities to sort the issues out, `
Justice Bhushan read out from the order.

The bench also passed the following directions :
1. **A date for conciliation and settlement may be effectuated by the MHA.**

2. **Directions for participation of employees for effectuating this settlement may be publicised for the benefit of all employers and workers.**

3. **State governments to facilitate such settlements and initiate the process for the same and submit reports before the concerned labor commissioners.**

4. **Workers who are willing to work should be allowed to work notwithstanding disputes regarding wages.**

The case will be next taken up during the last week of July. The SC direction against coercive action against employers on the MHA Order will continue till then.
Centre and states have been directed to circulate court order through labour departments to facilitate settlements.
The MHA direction was in force for 54 days, before it was withdrawn with effect from May 18.
Financial incapacity cannot be a reason to challenge the notification, the MHA said in its counter-affidavit.

`Financial incapacity is a legally untenable ground to challenge a direction
issued by a competent authority in the exercise of its statutory power, `
stated the MHA's counter affidavit.

The MHA explained that directions were a temporary measure to
"**mitigate the financial hardship of the employees and workers specially contractual and casual during the lockdown period.**"

The MHA also informed the Court that the March 29 notification has ceased to have with effect from May 18.

On May 15, the SC had passed an interim order in the petitions filed by Hand Tools Manufacturers Association and Jute Mills Association
that no coercive action should be taken against the petitioners for non-compliance of the MHA direction.

On May 26, the SC had issued extended the interim orders passed in the cases of Hand Tools Manufacturers Association and Jute Mills Association.

The MHA direction was challenged as unreasonable and arbitrary and as violative of the fundamental right to trade and business of the employers.
Since operations are completely shut down during the lockdown,
it is impossible for the employers to continue to bear the burden of full salary of employees.

**Download order here**:
[Employer Challenging Govt order to pay minimum wages_Judgement_12-Jun-2020](https://workingpeoplescharter.in/documents/15/Employer_Challenging_Govt_order_to_pay_minimum_wages_Judgement_12-Jun-2020.pdf)

 **Picture details and Credit**: CITU Nasik Samsonite Unit protest for 100% wages of lockdown period. Comrade Kiran Moghe CITU,

 **Summary of order credit** - LiveLaw Twitter Page
