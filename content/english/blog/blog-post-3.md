---
title: "LOCKDOWN DIARIES"
date: 2020-06-10T15:27:17+06:00
draft: false
# page title background image
bg_image: "images/backgrounds/page-title.jpg"
# meta description
description : "Migrants journey during lock down"
# post thumbnail
image: "images/blog/lockdown_diaries_3.jpg"
# post author
authors: ["Shweta Damle"]
# taxonomy
categories: []
tags: []
type: post

---
### The migrant question

The imagination of migrant workers in abstraction may lead to confusion. Well the first problem with identification of migrants as one entity is misleading.

There are a wide range of migrants -

- **some may be daily wagers,**

- **some changing their occupations seasonally,**

- **many operating in the economy of the poor**

There will also be migrants with very specialised skills.
Their returning or not returning to cities may not simply be based on their experiences during the lock down. It may be a short term factor.
Much desired would be if they get employment closer to their native places, but are their jobs there is the question that also needs to be thought. Is there enough development in the smaller towns and cities.

##### There is probably a need to define development, what kind of employment, can it be in harmony with the nature?

These are critical questions to think about.
Getting back to how the diversity of migrants is being homogenised by all media and few researchers too.
Let us ask these few critical questions for beginers and then come to whether migrants will return or no -

##### Is there work for the migrant worker in Mumbai?

How profitable will it be? If the profit margins are not too big is it worth coming back again?
How decent is the living arrangement ( this is relative. For those living in sheds on construction sites even a room in the slums is decent)
How much respect will the worker get back at home as well as here, more so in cases where workers have acquired decent skills.
Therefore migrants should not located in abstracts rather any analysis should be entrenched in their realities.
